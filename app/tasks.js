const express = require('express');
const Task = require('../models/Task');
const auth = require("../middleware/auth");

const router = express.Router();

router.post('/', auth, async (req, res) => {
	if (!req.body.title || !req.body.status) {
		return res.status(400).send({error: 'Data not valid'});
	}

	const taskData = {
		user: req.user._id,
		title: req.body.title,
		description: req.body.description || null,
		status: req.body.status
	};

	const task = new Task(taskData);

	const error = task.validateSync();

	if (error) {
		return res.status(400).send({error: error.errors['status'].message})
	}

	try {
		await task.save();
		res.send(task);
	} catch (e) {
		res.sendStatus(500);
	}
});

router.get('/', auth, async (req, res) => {
	try {
		const tasks = await Task.find({user: req.user._id});
		res.send(tasks);
	} catch (e) {
		res.sendStatus(500);
	}
});

router.put('/:id', auth, async (req, res) => {
	try {
		const task = await Task.findByIdAndUpdate(req.params.id, {
			$set: {
				title: req.body.title,
				description: req.body.description,
				status:  req.body.status
			}}, {'new': true});

		if (req.user._id.valueOf() !== task.user.valueOf()) {
			return res.status(403).send({error: `you don't have permission`});
		} else {
			await task.save();
			res.send(task);
		}
	} catch (e) {
		res.status(500);
	}
});

router.delete('/:id', auth, async (req, res) => {
	try {
		const task = await Task.findByIdAndDelete(req.params.id);

		if (task) {
			if (req.user._id.valueOf() !== task.user.valueOf()) {
				return res.status(403).send({error: `you don't have permission`});
			} else {
				res.send(`User removed!`);
			}
		} else {
			res.status(404).send({error: 'User not found'});
		}
	} catch (e) {
		res.sendStatus(500);
	}
});

module.exports = router;