const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TaskSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
	},
	title: {
		type: String,
		required: true,
	},
	description: String,
	status: {
		type: String,
		enum: ['new', 'in_progress', 'complete'],
		message: '{VALUE} is not supported',
		required: true
	}
});

TaskSchema.plugin(idValidator);
const Task = mongoose.model('Task', TaskSchema);
module.exports = Task;